#include "petsc.h"
#include "petscfix.h"
/* matadic.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscda.h"
#include "petscsnes.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matdaadsetsnes_ MATDAADSETSNES
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matdaadsetsnes_ matdaadsetsnes
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcreatedaad_ MATCREATEDAAD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcreatedaad_ matcreatedaad
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matregisterdaad_ MATREGISTERDAAD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matregisterdaad_ matregisterdaad
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   matdaadsetsnes_(Mat A,SNES snes, int *__ierr ){
*__ierr = MatDAADSetSNES(
	(Mat)PetscToPointer((A) ),
	(SNES)PetscToPointer((snes) ));
}
void PETSC_STDCALL   matcreatedaad_(DA da,Mat *A, int *__ierr ){
*__ierr = MatCreateDAAD(
	(DA)PetscToPointer((da) ),A);
}
void PETSC_STDCALL   matregisterdaad_(int *__ierr ){
*__ierr = MatRegisterDAAD();
}
#if defined(__cplusplus)
}
#endif

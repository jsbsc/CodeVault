# README - Boost.MPI and OpenMP (Naive n-Body Example)

## Description

This sample is a simple particle simulation in C++ 14 using Boost.MPI with OpenMP. The code is explicitly not optmized for better understanding. The idea is to run one MPI process per compute nodes. Within a node, the simulation is parallelized using OpenMP.

This code sample demonstates:
 * How to setup Boost.MPI
 * **collective communication**, i.e. `boost::mpi::broadcast`
 * How to use Boost.MPI serialization to **send and receive non-contiguous structured data**
 
 The code sample is structures as followed:
 
  * `Configuration.hpp`: Configuration structure, serialization, command-line parsing.
  * `main.cpp`: The main program.
  * `Particle.hpp`: Particle structure, serialization.
  * `Vec3.hpp`: 3D-Vector structure, arithmetrical operators, serialization
  * `gsl/*`: Guideline Support Library [C++ Core Guidelines](http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines), is a utility library. (https://github.com/Microsoft/GSL)
  
## Release Date

2016-07-27


## Version History

 * 2016-07-27 Initial Release on PRACE CodeVault repository


## Contributors

 * Thomas Steinreiter - [thomas.steinreiter@risc-software.at](mailto:thomas.steinreiter@risc-software.at)


## Copyright

This code is available under Apache License, Version 2.0 - see also the license file in the CodeVault root directory. The GSL library is available under MIT.


## Languages

This sample is written in C++ 14.


## Parallelisation

This sample uses MPI-3 and OpenMP-2 for parallelisation.


## Level of the code sample complexity

Intermediate / Advanced


## Compiling

Follow the compilation instructions given in the main directory of the kernel samples directory (`/hpc_kernel_samples/README.md`). Note: make sure, that Boost is built with  the correct compiler version, Boost.MPI is enabled and configured with the correct MPI installation. (`http://www.boost.org/doc/libs/1_58_0/doc/html/mpi/getting_started.html#mpi.config``)

## Running

To run the program, use something similar to

    mpiexec -n [nprocs] ./4_nbody_naive__bmpi_omp -i [ni] -p [np]

either on the command line or in your batch script, where `ni` specifies the number of iterations and `np` specify the number of particles. If missing, the value `1000` is used.


### Command line arguments

 * `-i [iterations]`: specify the number of iterations, which are simulated
 * `-p [particles]`: specify the number of particles in the simulation
 
### Example
 
If you run

	mpiexec -n 4 ./4_nbody_naive__bmpi_omp -p 1000 -i 1000
	
the output should look similar to

	Initialization done:
      0X:  -0.8817675
      1X: 0.007429462
      2X:  -0.8294838
      3X:   0.2370442
      4X:   0.3821877
      5X:    -0.28594
      6X:  -0.5633672
      7X:   0.8819869
      8X:  -0.3698202
      9X:   0.1124098
	End Simulation
      0X:  -0.9042547
      1X:  0.01078305
      2X:  -0.8496572
      3X:   0.2430004
      4X:   0.3865536
      5X:  -0.2747932
      6X:  -0.5706735
      7X:    0.910435
      8X:  -0.3810537
      9X:   0.1221019
	
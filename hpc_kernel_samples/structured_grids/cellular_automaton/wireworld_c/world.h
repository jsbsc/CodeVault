#ifndef _WORLD_H_
#define _WORLD_H_

#include <stddef.h>
#include <mpi.h>

#include "configuration.h"

typedef struct
{
   size_t n_neighbors;

   int neighbor_ranks[8];
   MPI_Datatype send_types[8];
   MPI_Datatype recv_types[8];

   MPI_Datatype io_type;

   MPI_Request persistent_requests[16];
   MPI_Comm comm;
} transfer_t;

typedef struct
{
   size_t global_size[2];
   size_t local_size[2];
   size_t local_start[2];

   char *cells_prev;
   char *cells_next;

   transfer_t transfer;
} world_t;

void world_init(world_t *world, size_t *global_size, const conf_t *c);

size_t world_get_storage_size(const world_t *world);

void world_free(world_t *world);

#endif


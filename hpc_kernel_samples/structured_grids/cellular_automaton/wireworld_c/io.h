#ifndef _IO_H_
#define _IO_H_

#include <stddef.h>
#include <mpi.h>

#include "world.h"

void file_read_header(MPI_File file, size_t *global_size, size_t *header_length);

void file_write_header(MPI_File file, const world_t *world, size_t *header_length);

void file_read_world(MPI_File file, world_t *world, size_t header_length);

void file_write_world(MPI_File file, const world_t *world, size_t header_length);

#endif

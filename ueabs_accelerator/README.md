Application performance on accelerators
=======================================

This is an extention of UEABS (Unified European Application Benchmark Suite) for accelerators. It is composed of a set of 11 codes that includes 1 synthetic benchmark and 10 commonly used applications. The key focus of this task has been exploiting accelerators or co-processors to improve the performance of real applications. It aims at providing a set of scalable, currently relevant and publically available codes and datasets.

This work has been undertaken by Task7.2B "Accelerator Benchmarks" in the PRACE Fourth Implementation Phase (PRACE-4IP) project.

Most of the selected application are a subset of UEABS. Exceptions are PFARM which comes from PRACE-2IP and SHOC the synthetic benchmark suite.

For each code, namly Alya, Code_Saturne, CP2K, GROMACS, GPAW, NAMD, PFARM, QCD, Quantum Espresso, SHOC and SPECFEM3D, either two or more test case datasets have been selected. There are described in details in the PRACE deliverable D7.5 of the forth implementation.

Running the suite
-----------------

Inscructions to run each test cases of each codes can be found in the subdirectories of this repository.

